const {
    agregarUsuario: modeAgregarUsuario,
    obtenerListaU: modeObtenerListaU,
    editarUsuario: modeEditarU,
    buscarUsuario:modeBuscarU,
    eliminarUsuario: modeEliminarU,
} = require("../models/usuarios");

function agregarUsuario(usuario) {
    //llamar a la capa modelo
    return modeAgregarUsuario(usuario).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
function obtenerListaU() {
    return modeObtenerListaU('usuarios.json').then(JSON.parse)
        .then(resultado => {
            return {
                status: 200, body: resultado
            }
        })
}

function buscarUsuario(usuario) {
    return modeBuscarU(usuario).then(resultado => {
            return {
                status: 200, body: resultado
            }
        })
}

function editarUsuario(usuario,datosReq) {
    return modeEditarU(usuario,datosReq).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
function eliminarUsuario(usuario) {
    return modeEliminarU(usuario).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
module.exports = {
    agregarUsuario,
    obtenerListaU,
    editarUsuario,
    buscarUsuario,
    eliminarUsuario
};