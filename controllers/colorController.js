const {
    agregarColor: modeAgregarColor,
    obtenerLista: modeObtenerLista,
    editarColor: modeEditar,
    buscarColor:modeBuscar,
    eliminarColor: modeEliminar,
} = require("../models/colores");

function agregarColor(color) {
    //llamar a la capa modelo
    return modeAgregarColor(color).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
function obtenerLista() {
    return modeObtenerLista('recursos.json').then(JSON.parse)
        .then(resultado => {
            return {
                status: 200, body: resultado
            }
        })
}

function buscarColor(color) {
    return modeBuscar(color).then(resultado => {
            return {
                status: 200, body: resultado
            }
        })
}

function editarColor(color,datosReq) {
    return modeEditar(color,datosReq).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
function eliminarColor(color) {
    return modeEliminar(color).then(resultado => {
        return {
            status: 200, body: { mensaje: resultado }
        }
    })
}
module.exports = {
    agregarColor,
    obtenerLista,
    editarColor,
    buscarColor,
    eliminarColor
};