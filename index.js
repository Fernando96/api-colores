const express = require('express');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
const bodyParser = require('body-parser');
const coloresRouter = require('./routers/colores');
const usuariosRouter = require('./routers/usuarios');
const fs = require("fs");

const app = express();
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.use(bodyParser.json());

var usuarios = [];
var jwtClave = "laclave";



app.use('/colores', coloresRouter);

app.use('/usuarios', usuariosRouter);

const usuarioLogeado=[{nombre:"",contrasena:""}];

app.post("/login", function (request, response) {
  let usuariosList = fs.readFileSync('usuarios.json');
  usuarios = JSON.parse(usuariosList);
  for (let i = 0; i < usuarios.length; i++) {
    if (request.body.nombre == usuarios[i].nombre && request.body.contrasena == usuarios[i].contrasena) {

      var token = jwt.sign({
        user: usuarios[i].nombre
      }, jwtClave);

      response.status(200).json({
        message: 'Login satisfactorio',
        usuario: usuarios[i].nombre,
        jwtoken: token
      });
      usuarioLogeado.splice(0, 1);
      usuarioLogeado.push(usuarios[i]);

    }
  }
  response.status(401).end("Error login. Usuario o contraseña incorrectos")

});


app.listen(process.env.PUERTO, () => {
  console.log("Escuchando en el puerto " + process.env.PUERTO)
});

module.exports.usuarioLogeado = usuarioLogeado;