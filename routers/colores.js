var express = require('express');
var router = express.Router();
var expressJwt = require('express-jwt');
//declaracion
const {
    agregarColor,
    obtenerLista,
    buscarColor,
    editarColor,
    eliminarColor
} = require('../controllers/colorController');

var jwtClave = "laclave";



//inicamos un enrutador
router.get('/', function (req, res) {
    obtenerLista().then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});
router.get('/:color', function (req, res) {
    buscarColor(req.params.color).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});


router.post('/', function (req, res) {
    agregarColor(req.body).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});

router.use(expressJwt({ secret: jwtClave }).unless({ path: ["/login"] }));

router.put('/:color', function (req, res) {
    editarColor(req.params.color,req.body).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    })

});

router.delete('/:color', function (req, res) {
    eliminarColor(req.params.color).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    })

});

module.exports = router;