var express = require('express');
var router = express.Router();
//declaracion
const {
    agregarUsuario,
    obtenerListaU,
    buscarUsuario,
    editarUsuario,
    eliminarUsuario
} = require('../controllers/usuarioController');

//inicamos un enrutador
router.get('/', function (req, res) {
    obtenerListaU().then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});
router.get('/:usuario', function (req, res) {
    buscarUsuario(req.params.usuario).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});


router.post('/', function (req, res) {
    agregarUsuario(req.body).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    });
});

router.put('/:usuario', function (req, res) {
    editarUsuario(req.params.usuario,req.body).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    })

});

router.delete('/:usuario', function (req, res) {
    eliminarUsuario(req.params.usuario).then(resultado => {
        res.status(resultado.status);
        res.json(resultado.body);
    })

});

module.exports = router;