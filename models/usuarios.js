let usuarios = [];
let recursos = [];
const fs = require("fs");

function agregarUsuario(usuario) {

    return new Promise((resolve, reject) => {
        //cargar los datos existentes
        usuarios = require('../usuarios.json');

        //se guardan los datos dentro del arreglo 
        usuarios.push(usuario);
        fs.writeFileSync('usuarios.json', JSON.stringify(usuarios));

        //mensaje de respuesta
        resolve("Ingresado Correctamente")
    });
}

function obtenerListaU(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return
            }

            resolve(data);
        });
    });

}

function buscarUsuario(usuario) {
    return new Promise((resolve, reject) => {

        let datosRaw = fs.readFileSync('usuarios.json');
        usuarios = JSON.parse(datosRaw);
        for (let i = 0; i < usuarios.length; i++) {
            if (usuarios[i].nombre == usuario) {
                resolve(usuarios[i]);

            }

        }
        resolve("usuario " + usuario + " no fue encontrado")

    });
}

function editarUsuario(usuario, datosReq) {
    return new Promise((resolve, reject) => {


        let datosRaw = fs.readFileSync('usuarios.json');
        usuarios = JSON.parse(datosRaw);
        for (let i = 0; i < usuarios.length; i++) {
            if (usuarios[i].nombre == usuario) {
                usuarios[i] = datosReq;
                fs.writeFileSync('usuarios.json', JSON.stringify(usuarios));
                resolve('El usuario fue editado');

            }

        }
        resolve("usuario " + usuario + " no fue encontrado")

    });
}

function eliminarUsuario(usuario) {
    return new Promise((resolve, reject) => {

        let datosRaw = fs.readFileSync('usuarios.json');
        usuarios = JSON.parse(datosRaw);
        for (let i = 0; i < usuarios.length; i++) {
            let ListaColores = fs.readFileSync('recursos.json');
            recursos = JSON.parse(ListaColores);
            for (let j = 0; j < recursos.length; j++) {
                if (recursos[j].creador == usuarios[i].nombre) {
                    recursos.splice(j, 1);
                    fs.writeFileSync('recursos.json', JSON.stringify(recursos));
                    j--;
                    console.log("encontro")
                }

            }
            if (usuarios[i].nombre == usuario) {
                usuarios.splice(i, 1);
                fs.writeFileSync('usuarios.json', JSON.stringify(usuarios));
                resolve('El usuario ' + usuario + ' y sus colores asociados fueron eliminados');

            }

        }
        resolve("usuario " + usuario + " no encontrado")

    });
}



module.exports = {
    agregarUsuario,
    obtenerListaU,
    buscarUsuario,
    eliminarUsuario,
    editarUsuario
}
