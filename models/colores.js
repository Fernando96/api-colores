let recursos = [];
const usuarioLogeado = require('../index');
const fs = require("fs");

function agregarColor(color) {

    return new Promise((resolve, reject) => {
        //cargar los datos existentes
        recursos = require('../recursos.json');
        if (usuarioLogeado.usuarioLogeado[0].nombre == '') {
            var colorFinal = {
                color: color.color,
                rgb: color.rgb,
                creador: "Anonimo"
            };
        } else {
            var colorFinal = {
                color: color.color,
                rgb: color.rgb,
                creador: usuarioLogeado.usuarioLogeado[0].nombre
            };
        }

        //se guardan los datos dentro del arreglo 
        recursos.push(colorFinal);
        fs.writeFileSync('recursos.json', JSON.stringify(recursos));

        //mensaje de respuesta
        resolve("Ingresado Correctamente")
    });
}

function obtenerLista(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return
            }

            resolve(data);
        });
    });

}

function buscarColor(color) {
    return new Promise((resolve, reject) => {

        let datosRaw = fs.readFileSync('recursos.json');
        recursos = JSON.parse(datosRaw);
        for (let i = 0; i < recursos.length; i++) {
            if (recursos[i].color == color) {
                resolve(recursos[i]);

            }

        }
        resolve("Color " + color + " no fue encontrado")

    });
}

function editarColor(color, datosReq) {
    return new Promise((resolve, reject) => {

       
       

        let datosRaw = fs.readFileSync('recursos.json');

        recursos = JSON.parse(datosRaw);
        for (let i = 0; i < recursos.length; i++) {
            if (recursos[i].color == color && (recursos[i].creador == usuarioLogeado.usuarioLogeado[0].nombre || recursos[i].creador == "Anonimo") && usuarioLogeado.usuarioLogeado[0].nombre != '') {
                
                if(recursos[i].creador=="Anonimo"){
                    var datosReqFinal = {
                        color: datosReq.color,
                        rgb: datosReq.rgb,
                        creador: recursos[i].creador
                    };
                }else{
                    var datosReqFinal = {
                        color: datosReq.color,
                        rgb: datosReq.rgb,
                        creador: usuarioLogeado.usuarioLogeado[0].nombre
                    };
                }

                recursos[i] = datosReqFinal;
                fs.writeFileSync('recursos.json', JSON.stringify(recursos));
                resolve('El color fue editado');

            }
            if (recursos[i].color == color && usuarioLogeado.usuarioLogeado[0].nombre == '') {
                resolve('El color no puede ser modificado, sin estar logeado');

            }
            if (recursos[i].color == color && recursos[i].creador != usuarioLogeado.usuarioLogeado[0].nombre) {
                resolve('El color no puede ser modificado, ya que el usuario logeado no fue el creador');

            }

        }
        resolve("Color " + color + " no fue encontrado")

    });
}

function eliminarColor(color) {
    return new Promise((resolve, reject) => {
        let datosRaw = fs.readFileSync('recursos.json');
        recursos = JSON.parse(datosRaw);
        for (let i = 0; i < recursos.length; i++) {
            if (recursos[i].color == color && (recursos[i].creador == usuarioLogeado.usuarioLogeado[0].nombre || recursos[i].creador == "Anonimo") && usuarioLogeado.usuarioLogeado[0].nombre != '') {
                recursos.splice(i, 1);
                fs.writeFileSync('recursos.json', JSON.stringify(recursos));
                resolve('El color ' + color + ' fue eliminado');

            }
            if (recursos[i].color == color && usuarioLogeado.usuarioLogeado[0].nombre == '') {
                resolve('El color no puede ser eliminado, sin estar logeado');

            }
            if (recursos[i].color == color && recursos[i].creador != usuarioLogeado.usuarioLogeado[0].nombre) {
                resolve('El color no puede ser eliminado, ya que el usuario logeado no fue el creador');

            }


        }
        resolve("Color " + color + " no encontrado")

    });
}



module.exports = {
    agregarColor,
    obtenerLista,
    buscarColor,
    eliminarColor,
    editarColor
}
