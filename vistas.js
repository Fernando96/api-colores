
var storage = window.localStorage;

function Login() {
    var datos = {
        nombre: document.getElementById("nombre").value,
        contrasena: document.getElementById("contrasena").value,
    };

    $.ajax({
        url: 'http://localhost:3000/login',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(datos),
        success: function (data) {
           
            storage.setItem("token", data.jwtoken);
            storage.setItem("nombre", data.usuario);

            window.location.href = 'index.html';
        }, error: function (data) {
            document.getElementById('mensaje').innerHTML = data.responseText;

        }

    });
}

function IngresarColor() {
    var datos = {
        color: document.getElementById("color").value,
        rgb: document.getElementById("rgb").value,
    };

    if(datos.color=="" || datos.color==""){
        document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">Los campos estan vacios</div>';
    }else{
        $.ajax({
            url: 'http://localhost:3000/colores',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "Authorization":
                  "Bearer " + storage.getItem("token"),
              },
            data: JSON.stringify(datos),
            success: function (data) {
    
                 document.getElementById("color").value='';
                 document.getElementById("rgb").value='';
                document.getElementById("tablaColores").innerHTML= '';
                TablaColores();
                document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">'+data.mensaje+'</div>';
            }, error: function (data) {
                document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">Error</div>';
    
            }
    
        });
    }

   
}

var coloParaEditar;
function verColorEditar(Nombrecolor){
    coloParaEditar=Nombrecolor;
    console.log(Nombrecolor)
    $.ajax({
        url: 'http://localhost:3000/colores/'+Nombrecolor+'/',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
            document.getElementById("editarForm").style="";
            document.getElementById("colorED").value=data.color;
            document.getElementById("rgbED").value=data.rgb;
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}

function verColor(Nombrecolor){
    $.ajax({
        url: 'http://localhost:3000/colores/'+Nombrecolor+'/',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
            document.getElementById("verColor").style="";
            document.getElementById("verColorN").innerHTML=data.color;
            document.getElementById("verColorR").innerHTML=data.rgb;
            document.getElementById("verColorC").innerHTML=data.creador;
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}

function EditarColor() {
    var datos = {
        color: document.getElementById("colorED").value,
        rgb: document.getElementById("rgbED").value,
    };
    console.log(datos)
    if(datos.color=="" || datos.color==""){
        document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">Los campos estan vacios</div>';
    }else{
        console.log(coloParaEditar)
        $.ajax({
            url: 'http://localhost:3000/colores/'+coloParaEditar+'/',
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "Authorization":
                  "Bearer " + storage.getItem("token"),
              },
            data: JSON.stringify(datos),
            success: function (data) {
                
                document.getElementById("editarForm").style="visibility: hidden;";
                 
                document.getElementById("tablaColores").innerHTML= '';
                TablaColores();
                document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">'+data.mensaje+'</div>';
            }, error: function (data) {
                document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">Para editar un color debe inicar sesión</div>';
    
            }
    
        });
    }

   
}

$(document).ready(function(){
    TablaColores();
    TablaUsuarios();
    
});

function TablaColores(){
    $.ajax({
        url: 'http://localhost:3000/colores',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
           
           
           for (let i = 0; i < data.length; i++) {
              
               var color="'"+data[i].color+"'";
               var tabla;
                      /* 
                if(storage.getItem("token")!=null){
                    tabla=' <tr>'+
                    '<th scope="row">'+(i+1)+'</th>'+
                    '<td>'+data[i].color+'</td>'+
                    '<td>'+data[i].rgb+'</td>'+
                    '<td>'+data[i].creador+'</td>'+
                    '<td>'+
                         '<a type="submit" class="btn btn-primary" onclick="verColor('+color+')" style="color: white;">Ver</a> '+
                         '<a type="submit" class="btn btn-warning" onclick="verColorEditar('+color+')" style="color: white;">Editar</a> '+ 
                         '<a type="submit" class="btn btn-danger" onclick="EliminarColor('+color+')" style="color: white;">Eliminar</a>'+
                     '</td>'
                  '</tr>';
                }else{
                    tabla=' <tr>'+
                    '<th scope="row">'+(i+1)+'</th>'+
                    '<td>'+data[i].color+'</td>'+
                    '<td>'+data[i].rgb+'</td>'+
                    '<td>'+data[i].creador+'</td>'+
                    '<td>'+
                         '<a type="submit" class="btn btn-primary" onclick="verColor('+color+')" style="color: white;">Ver</a> '+
                     
                     '</td>'
                  '</tr>';
                }*/
               tabla=' <tr>'+
               '<th scope="row">'+(i+1)+'</th>'+
               '<td>'+data[i].color+'</td>'+
               '<td>'+data[i].rgb+'</td>'+
               '<td>'+data[i].creador+'</td>'+
               '<td>'+
                    '<a type="submit" class="btn btn-primary" onclick="verColor('+color+')" style="color: white;">Ver</a> '+
                    '<a type="submit" class="btn btn-warning" onclick="verColorEditar('+color+')" style="color: white;">Editar</a> '+ 
                    '<a type="submit" class="btn btn-danger" onclick="EliminarColor('+color+')" style="color: white;">Eliminar</a>'+
                '</td>'
             '</tr>';
        
            document.getElementById("tablaColores").innerHTML=  document.getElementById("tablaColores").innerHTML+tabla;
           }
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}

function EliminarColor(coloHaEliminar){

    $.ajax({
        url: 'http://localhost:3000/colores/'+coloHaEliminar+'/',
        type: 'DELETE',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
        
           
            document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">'+data.mensaje+'</div>';
            document.getElementById("tablaColores").innerHTML= '';
            TablaColores();
        }, error: function (data) {
            document.getElementById('mensajeColor').innerHTML = '<div class="alert alert-primary">Para eliminar un color debe inicar sesión</div>';
    

        }

    });
}



if (storage.getItem("token")!=null) {
    document.getElementById("BotonU").style="";
    document.getElementById("BotonIL").href='index.html'
    document.getElementById("BotonIL").innerHTML=storage.getItem("nombre")+" Cerrar sesión";

    document.getElementById ('BotonIL').onclick = function(){
        storage.removeItem("token");
        storage.removeItem("nombre");
       }

}

function CerrarColor(){
    document.getElementById("verColor").style="visibility: hidden;";
}

function CerrarEditar(){
    document.getElementById("editarForm").style="visibility: hidden;";
}



function registro() {
    var datos = {
        nombre: document.getElementById("nombreReg").value,
        contrasena: document.getElementById("contrasenaReg").value,
    };

    if(datos.nombre=="" || datos.contrasena==""){
        document.getElementById('mensaje').innerHTML = 'Los campos estan vacios';
    }else{

    $.ajax({
        url: 'http://localhost:3000/usuarios',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(datos),
        success: function (data) {
           document.getElementById("nombreReg").value="";
            document.getElementById("contrasenaReg").value="";
            document.getElementById('mensaje').innerHTML = data.mensaje;
            document.getElementById("tablaUsuarios").innerHTML= '';
                TablaUsuarios();
        }, error: function (data) {
            document.getElementById('mensaje').innerHTML = data.responseText;

        }

    });
}
}

function TablaUsuarios(){
    $.ajax({
        url: 'http://localhost:3000/usuarios',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
           
           
           for (let i = 0; i < data.length; i++) {
              
               var usuario="'"+data[i].nombre+"'";
               var tabla;
            
               tabla=' <tr>'+
               '<th scope="row">'+(i+1)+'</th>'+
               '<td>'+data[i].nombre+'</td>'+
               
               '<td>'+
                    '<a type="submit" class="btn btn-warning" onclick="verUsuarioEditar('+usuario+')" style="color: white;">Editar</a> '+ 
                    '<a type="submit" class="btn btn-danger" onclick="EliminarUsuario('+usuario+')" style="color: white;">Eliminar</a>'+
                '</td>'
             '</tr>';
        
            document.getElementById("tablaUsuarios").innerHTML=  document.getElementById("tablaUsuarios").innerHTML+tabla;
           }
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}


var usuarioParaEditar;
function verUsuarioEditar(Nombreusuario){
    usuarioParaEditar=Nombreusuario;
    console.log(Nombreusuario)
    $.ajax({
        url: 'http://localhost:3000/usuarios/'+Nombreusuario+'/',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
            document.getElementById("editarForm").style="";
            document.getElementById("usuarioED").value=data.nombre;
            document.getElementById("contraseniaED").value=data.contrasena;
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}

function verUsuario(Nombreusuario){
    $.ajax({
        url: 'http://localhost:3000/usuarios/'+Nombreusuario+'/',
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
            document.getElementById("verColor").style="";
            document.getElementById("verColorN").innerHTML=data.color;
            document.getElementById("verColorR").innerHTML=data.rgb;
            document.getElementById("verColorC").innerHTML=data.creador;
          
        }, error: function (data) {
            console.log( "Error");

        }

    });
}

function EditarUsuario() {
    var datos = {
        nombre: document.getElementById("usuarioED").value,
        contrasena: document.getElementById("contraseniaED").value,
    };
    console.log(datos)
    if(datos.nombre=="" || datos.contrasena==""){
        document.getElementById('mensaje').innerHTML = '<div class="alert alert-primary">Los campos estan vacios</div>';
    }else{
        console.log(usuarioParaEditar)
        $.ajax({
            url: 'http://localhost:3000/usuarios/'+usuarioParaEditar+'/',
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "Authorization":
                  "Bearer " + storage.getItem("token"),
              },
            data: JSON.stringify(datos),
            success: function (data) {
                
                document.getElementById("editarForm").style="visibility: hidden;";
                 
                document.getElementById("tablaUsuarios").innerHTML= '';
                TablaUsuarios();
                document.getElementById('mensaje').innerHTML = '<div class="alert alert-primary">'+data.mensaje+'</div>';
            }, error: function (data) {
                document.getElementById('mensaje').innerHTML = '<div class="alert alert-primary">Para editar un usuario debe inicar sesión</div>';
    
            }
    
        });
    }

   
}

function EliminarUsuario(usuarioHaEliminar){

    $.ajax({
        url: 'http://localhost:3000/usuarios/'+usuarioHaEliminar+'/',
        type: 'DELETE',
        dataType: 'json',
        contentType: 'application/json',
        headers: {
            "Authorization":
              "Bearer " + storage.getItem("token"),
          },
      
        success: function (data) {
        
           
            document.getElementById('mensaje').innerHTML = '<div class="alert alert-primary">'+data.mensaje+'</div>';
            document.getElementById("tablaUsuarios").innerHTML= '';
            TablaUsuarios();
        }, error: function (data) {
            document.getElementById('mensaje').innerHTML = '<div class="alert alert-primary">Para eliminar un usuario debe inicar sesión</div>';
    

        }

    });
}